<?php

namespace app;

use app\models\MarketEstateLinkPrice;
use app\parsing\parser\BaseListObject;
use app\parsing\parser\BasePage;
use app\parsing\parser\BaseObject;
use app\parsing\parser\BesplatkaObject;
use app\parsing\parser\DomRiaObject;
use app\parsing\parser\DomRiaPage;
use app\parsing\parser\FnObject;
use app\parsing\parser\FnPage;
use app\parsing\parser\Image;
use app\parsing\parser\OlxObject;
use app\parsing\parser\OlxPage;
use app\parsing\parser\Realty100Object;
use app\parsing\parser\Realty100Page;
use app\parsing\parser\ReLvivObject;
use app\parsing\parser\ReLvivPage;
use app\parsing\parser\RieltorObject;
use app\parsing\parser\RieltorPage;
use Yii;
use app\models\Link;
use app\models\ParsingPortal;
use yii\base\Exception;
use \yii\bootstrap\Html;
use app\models\ParsingError;
use app\models\Phone;
use \app\models\Catalog;
use \app\models\MarketEstate;
use \app\models\MarketEstateImage;
use \app\models\MarketEstateLink;
use \app\models\MarketEstateLinkPhone;
use yii\db\Transaction;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

class Parsing2 {

    public $output = true;

    /**
     * @deprecated
     * @param $string
     * @param array $htmlOptions
     * @param null|string $consoleColor
     *
     * цвета для консоли здесь: @link https://habrahabr.ru/post/119436/
     *
     * \e[0m     все атрибуты по умолчанию
     * \e[30m    чёрный цвет знаков
     * \e[31m    красный цвет знаков
     * \e[32m    зелёный цвет знаков
     * \e[33m    желтый цвет знаков
     * \e[34m    синий цвет знаков
     * \e[35m    фиолетовый цвет знаков
     * \e[36m    цвет морской волны знаков
     * \e[37m    серый цвет знаков
     */
    public function output($string, $htmlOptions = [], $consoleColor = null)
    {
        if ($this->output === true) {
            if (\Yii::$app->id == 'basic-console') {
                if ($consoleColor)
                    echo "{$consoleColor}{$string}\e[0m ";
                else
                    echo $string . ' ';
            } else {
                $string = str_replace("\n", '<br>', $string);
                if ($htmlOptions) {
                    echo Html::tag('span', $string, $htmlOptions) . ' ';
                } else {
                    echo $string . ' ';
                }
                ob_flush();
                flush();
            }
        }
    }

    /**
     * @param string $from - parsing_portal_id or url
     * @return BasePage|null
     */
    public static function getPageParser ($from)
    {
        if (filter_var($from, FILTER_VALIDATE_URL)) {
            return self::getPageParserByUrl($from);
        } else {
            return self::getPageParserById($from);
        }
    }

    /**
     * @param $portal
     * @return BasePage|null
     */
    public static function getPageParserById ($portal)
    {
        switch($portal) {
            case ParsingPortal::OLX:
                $parser = new OlxPage();
                break;
            case ParsingPortal::FN:
                $parser = new FnPage();
                break;
            case ParsingPortal::REALTY100:
                $parser = new Realty100Page();
                break;
            case ParsingPortal::DOM_RIA:
                $parser = new DomRiaPage();
                break;
            case ParsingPortal::RIELTOR:
                $parser = new RieltorPage();
                break;
            case ParsingPortal::RE_LVIV:
                $parser = new ReLvivPage();
                break;
            default:
                $parser = null;
        }

        return $parser;
    }

    /**
     * @param $url
     * @return BasePage|null
     */
    public static function getPageParserByUrl ($url)
    {
        $parsingPortalId = ParsingPortal::urlToPortalId($url);
        $parser = self::getPageParserById($parsingPortalId);
        $parser->setUrl($url);
        return $parser;
    }

    /**
     * @param string $from - parsing_portal_id or url
     * @return BaseObject|null
     */
    public static function getObjectParser ($from)
    {
        if (filter_var($from, FILTER_VALIDATE_URL)) {
            return self::getObjectParserByUrl($from);
        } else {
            return self::getObjectParserById($from);
        }
    }

    /**
     * @param $portal
     * @return BaseObject|null
     */
    public static function getObjectParserById ($portal)
    {
        switch($portal) {
            case ParsingPortal::OLX:
                $parser = new OlxObject();
                break;
            case ParsingPortal::FN:
                $parser = new FnObject();
                break;
            case ParsingPortal::REALTY100:
                $parser = new Realty100Object();
                break;
            case ParsingPortal::DOM_RIA:
                $parser = new DomRiaObject();
                break;
            case ParsingPortal::RIELTOR:
                $parser = new RieltorObject();
                break;
            case ParsingPortal::RE_LVIV:
                $parser = new ReLvivObject();
                break;
            case ParsingPortal::BESPLATKA:
                $parser = new BesplatkaObject();
                break;
            default:
                $parser = null;
        }

        return $parser;
    }

    /**
     * @param $url
     * @return BaseObject|null
     */
    public static function getObjectParserByUrl ($url)
    {
        $parsingPortalId = ParsingPortal::urlToPortalId($url);
        $parser = self::getObjectParserById($parsingPortalId);
        $parser->setUrl($url);
        return $parser;
    }

    /**
     * сохраняет ссылки по каталогам
     * @param Catalog $catalog
     * @param int $maxItems
     * @return int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function catalogObjects(Catalog $catalog, $maxItems = 5000)
    {
        $newCount = 0;
        $itemsParsed = 0;
        $existedRecordsCount = 0;

        /**
         * @var int $k
         * @var BaseListObject $listObject */
        foreach ($catalog->getIterator() as $k => $listObject) {

            if (!$listObject->isValid) {
                stdout('invalid content');
                continue;
            }

            if ($catalog->parsing_portal_id == ParsingPortal::OLX) { // временная заглушка для OLX, убрать в мае 2019 XD

                /** @var Link $link */
                $link = Link::find()->where(['parsing_portal_id' => $catalog->parsing_portal_id, 'object_key' => $listObject->object_key])->one();
                if (!$link) {
                    /** @var Link $link */
                    $link = Link::find()->where(['url' => $listObject->url])->one();
                    if ($link) {
                        $link->updateAttributes(['object_key' => $listObject->object_key]);
                    }
                } else {

                    $otherLink = Link::find()->where(['url' => $listObject->url])->andWhere(['<>', 'id', $link->id])->one();
                    if ($otherLink) {
                        if ($otherLink->marketEstateLink) {
                            $otherLink->marketEstateLink->updateAttributes(['url' => null]);
                        }
                        $otherLink->delete();
                    }
                }
            } else {
                /** @var Link $link */
                $link = Link::find()->where(['parsing_portal_id' => $catalog->parsing_portal_id, 'object_key' => $listObject->object_key])->one();
            }

            if ($link) {
                if ($link->is_closed) {
                    $link->updateAttributes(['is_closed' => 0, 'url' => $listObject->url]);
                    $this->output('r', [], "\e[32m"); // reopened
                    $existedRecordsCount = 0;
                } else {
                    $this->output(".");
                    $existedRecordsCount++;
                }
                if ($link->marketEstateLink) {
                    $this->quickUpdate($link->marketEstateLink, $listObject);
                }
            } else {
                $link = new Link();
                $link->catalog_id = $catalog->id;
                $link->parsing_portal_id = $catalog->parsing_portal_id;
                $link->url = $listObject->url;
                $link->object_key = $listObject->object_key;
                $link->priority = $catalog->frequency ? $catalog->frequency : 0;

                if ($link->save()) {
                    $this->output('+', ['style' => 'color:green;'], "\e[32m");
                    $newCount++;
                } else {
                    $this->output(VarDumper::dumpAsString($link->firstErrors), [], "\e[31m");
//                        ParsingError::create($catalog->url, "Ошибка сохранения ссылки: {$url}, page: {$page}");
//                        $this->output('#', ['style' => 'color:red;', "\e[31m"]);
                }
                $existedRecordsCount = 0;
            }
            $itemsParsed++;

//            $this->output($itemsParsed);
            if ($itemsParsed >= $maxItems) {
                break;
            }
        }

        return $itemsParsed;
    }

    /**
     * @param MarketEstateLink $marketEstateLink
     * @param BaseListObject $listObject
     */
    public function quickUpdate(MarketEstateLink $marketEstateLink, BaseListObject $listObject)
    {
        if ($marketEstateLink) {

            try {

                $marketEstateLink->url = $listObject->url;
                $marketEstateLink->object_key = $listObject->object_key;
                $marketEstateLink->status = MarketEstateLink::STATUS_ACTIVE;
                $marketEstateLink->checked_at = time();
                $marketEstateLink->next_check_at = $marketEstateLink->checked_at + 60*60*12;

                if ($marketEstateLink->updated_at < strtotime(date('Y-m-d') . ' -7day')) {
                    $marketEstateLink->updated_at = $listObject->updated_at;
                }

                if ($listObject->price && $marketEstateLink->price != $listObject->price) {
                    $marketEstateLink->updated_at = $listObject->updated_at;

                    $marketEstateLink->price = $listObject->price;
                    $marketEstateLink->price_currency_id = $listObject->price_currency_id;
                    $marketEstateLink->price_kind = $listObject->price_kind;

                    if ($listObject->price_kind == 'per_object') {
                        $marketEstateLink->marketEstate->price_per_object = $listObject->price;
                        if ($marketEstateLink->marketEstate->area_total) {
                            $marketEstateLink->marketEstate->price_per_sqr = $listObject->price / $marketEstateLink->marketEstate->area_total;
                        }
                    } else {
                        $marketEstateLink->marketEstate->price_per_object = $marketEstateLink->marketEstate->area_total ? $listObject->price * $marketEstateLink->marketEstate->area_total : $listObject->price;
                        $marketEstateLink->marketEstate->price_per_sqr = $listObject->price;
                    }
                    $marketEstateLink->marketEstate->price_currency_id = $listObject->price_currency_id;
                    $marketEstateLink->marketEstate->save();

                    $marketEstateLinkPrice = new MarketEstateLinkPrice();
                    $marketEstateLinkPrice->market_estate_link_id = $marketEstateLink->id;
                    $marketEstateLinkPrice->created_at = time();
                    $marketEstateLinkPrice->price = $listObject->price;
                    $marketEstateLinkPrice->price_kind = $listObject->price_kind;
                    $marketEstateLinkPrice->price_currency_id = $listObject->price_currency_id;
                    if (!$marketEstateLinkPrice->save()) {
                        throw new Exception('update error: Parsing2::catalogObjects() $marketEstateLinkPrice->save() ' . VarDumper::dumpAsString($marketEstateLinkPrice->errors));
                    }
                }

                if ($marketEstateLink->save()) {
                    $marketEstateLink->marketEstate->setAttributes(['updated_at' => $marketEstateLink->updated_at, 'checked_at' => $marketEstateLink->checked_at]);
                    $marketEstateLink->marketEstate->save();
                }
                stdout('%yu%n ');
            } catch (\Exception $e) {
                stdout('%yue%n ');
                ParsingError::create($marketEstateLink->url, 'Parsing2::quickUpdate() ' . $e->getMessage());
            }

        }
    }

    /**
     * @param Link $link
     * @throws \yii\db\Exception
     */
    public function parseLink(Link $link)
    {
        /** @var Transaction $transaction */
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $this->parseUrl($link->url, $link->catalog, $link);
            $transaction->commit();
        } catch(Exception $e) {
            $transaction->rollBack();
            stdout("%r#%n");
            try {
                ParsingError::create($link->url, $e->getMessage() . "\n\n" . $e->getTraceAsString());
            } catch (\yii\db\Exception $e) {
                $this->output("#", ['style' => 'color:red;'], "\e[31m");
                ParsingError::create($link->url, 'Какието кривые символы в тексте, что их не смогло записать.');
            }
        }
    }

    /**
     * @param string $parsing_url
     * @param Catalog|null $catalog
     * @param Link|null $link
     * @param bool $checkSavedError
     * @return bool
     * @throws Exception
     */
    public function parseUrl(string $parsing_url, Catalog $catalog = null, Link $link = null, $checkSavedError = true)
    {
        /** @var BaseObject $parser */
        $parser = self::getObjectParser($parsing_url);

        if ($parser === null) {
            $link->updateAttributes(['is_bad' => 1, 'thread' => null]);
//            $this->output("@({$parser->parsing_portal_id})", ['style' => 'color:#b60072;font-weight: inherit;'], "\e[31m");
//            return false;
            throw new Exception('Parsing2::parseUrl() Не удалось определить парсер.');
        }

        if(ParsingError::hasError($parser->url) && $checkSavedError) {
            $link->updateAttributes(['is_bad' => 1, 'thread' => null]);
            $this->output("@({$parser->parsing_portal_id})", ['style' => 'color:#b60072;font-weight: inherit;'], "\e[31m");
            return false;
        }

        if ($parser->isClosed) {
            if ($link) {
                $link->updateAttributes(['is_closed' => 1, 'thread' => null]);
                if ($link->marketEstateLink) {
                    $link->marketEstateLink->updateAttributes(['status' => MarketEstateLink::STATUS_CLOSED]);
                }
            }
            $this->output("c({$parser->parsing_portal_id})", ['style' => 'background-color:green;'], "\e[42m");
            return false;
        }

        $parsingPortalId = ($catalog !== null) ? $catalog->parsing_portal_id : $parser->parsing_portal_id;

        if ($parsingPortalId == ParsingPortal::OLX) { // временная заглушка для OLX
            /** @var MarketEstateLink|null $model */
            $marketEstateLink = MarketEstateLink::findOne(['parsing_portal_id' => $parsingPortalId, 'object_key' => $parser->object_key]);
            if (!$marketEstateLink) {
                /** @var MarketEstateLink|null $model */
                $marketEstateLink = MarketEstateLink::findOne(['url' => $parser->url]);
            }
        } else {
            /** @var MarketEstateLink|null $model */
            $marketEstateLink = MarketEstateLink::findOne(['parsing_portal_id' => $parsingPortalId, 'object_key' => $parser->object_key]);
        }
        if($marketEstateLink) {
            if ($link) {
                $link->updateAttributes(['market_estate_link_id' => $marketEstateLink->id, 'thread' => null, 'object_key' => $parser->object_key]);
            }
            $this->output('.', ['style' => 'color:#00c031;'], "\e[33m");
            if ($catalog && !$marketEstateLink->catalog_id) {
                $marketEstateLink->catalog_id = $catalog->id;
            }
            if ($marketEstateLink->status == MarketEstateLink::STATUS_CLOSED) { // if object was closed earlie, then renew status and send to update
                $marketEstateLink->object_key = $parser->object_key;
                $marketEstateLink->status = MarketEstateLink::STATUS_ACTIVE;
                $marketEstateLink->next_check_at = time();
            }
            $marketEstateLink->object_key = $parser->object_key;
            $marketEstateLink->save();

            return true;
        }

        if (!$parser) {
            throw new Exception('Не удалось определить парсер.');
        }
        if ($catalog) {
            $parser->category = $catalog->category;
            $parser->realty_type = $catalog->realty_type;
            $parser->deal = $catalog->deal;
            $parser->region_id = $catalog->region_id;
        }

        if (empty($parser->phones) || empty($parser->realty_type) || empty($parser->price)) {
            if ($link) {
                $link->updateAttributes(['is_bad' => 1, 'thread' => null]);
            }
            $this->output("#({$parser->parsing_portal_id})", ['style' => 'color:red;', 'title' => $parser->url], "\e[31m");
            return false;
        }

        $marketEstate = MarketEstate::findTheSuitableOrCreate($parser);
        if (!$marketEstate) {
            $this->output('#(!$marketEstate)', ['style' => 'color:red;', 'title' => $parser->url], "\e[31m");
            return false;
        }

        $countImages = count($parser->images);
        $preview = null;
        $previewImg = null;
        if (!$marketEstate->preview) {
            $imagesData = [];
            foreach ($parser->images as $k => $img) {
                if (!(StringHelper::endsWith($img, '.jpg') || StringHelper::endsWith($img, '.jpeg') || StringHelper::endsWith($img, '.png'))) {
                    continue;
                }
                if (!$preview) {

                    $imageParser = new Image();
                    $imageParser->useProxy = $parser->parsing_portal_id == ParsingPortal::FN;
                    $preview = $imageParser->save($img, $parser->url);
                    if ($preview) {
                        $previewImg = $img;
                    }


//                    $preview = $parser->saveImage($img, $parser->url, 'amazon');
                }
                if ($previewImg != $img) {
                    $imagesData[] = [$img, $marketEstate->id, MarketEstateImage::STATUS_NEW];
                }

                if (count($imagesData) >= 6) break;
            }
            if ($imagesData) {
                Yii::$app->db->createCommand()->batchInsert('{{%market_estate_image}}', [
                    'source_url',
                    'market_estate_id',
                    'status',
                ], $imagesData)->execute();

            }
        }

        $marketEstateLink = new MarketEstateLink();
        if ($catalog) {
            $marketEstateLink->catalog_id = $catalog->id;
        }
        $marketEstateLink->url = $parser->url;
        $marketEstateLink->object_key = $parser->object_key;
        $marketEstateLink->parsing_portal_id = $parser->parsing_portal_id;
        $marketEstateLink->price = $parser->price;
        $marketEstateLink->price_kind = $parser->price_kind;
        $marketEstateLink->price_currency_id = $parser->price_currency_id;
        $marketEstateLink->contact_name = $parser->contact_name;
        $marketEstateLink->market_estate_id = $marketEstate->id;
        $marketEstateLink->status = MarketEstateLink::STATUS_ACTIVE;
        $marketEstateLink->contact_type = Phone::TYPE_OWNER; // здесь указывается сразу, ниже это значение будет апдейтится на базе телефонов.
        $marketEstateLink->count_views = $parser->portal_views;
        $marketEstateLink->count_images = $countImages;
        $marketEstateLink->created_at = $parser->portal_created_at;
        $marketEstateLink->updated_at = $parser->portal_updated_at;
        $marketEstateLink->checked_at = time();
        $marketEstateLink->next_check_at = $marketEstateLink->checked_at + 60*60*12;
        if (!$marketEstateLink->save()) {
            throw new Exception('update error: Parsing::catalog() $marketEstateLink->save() ' . VarDumper::dumpAsString($marketEstateLink->errors));
        }

        $isPhonesFromOwner = true;
        foreach ($parser->phones as $phone) {
            /** @var Phone $phone */
            $phone = Phone::findOrCreate($phone, $parser->phoneType);
            if ($isPhonesFromOwner)
                $isPhonesFromOwner = $phone->isOwner();

            if (!MarketEstateLinkPhone::find()->where(['market_estate_link_id' => $marketEstateLink->id, 'phone_id' => $phone->id])->exists()) {
                $marketEstateLinkPhone = new MarketEstateLinkPhone();
                $marketEstateLinkPhone->market_estate_link_id = $marketEstateLink->id;
                $marketEstateLinkPhone->market_estate_id = $marketEstate->id;
                $marketEstateLinkPhone->phone_id = $phone->id;
                $marketEstateLinkPhone->save();
            }
        }
        if (!$isPhonesFromOwner) {
            $marketEstateLink->updateAttributes(['contact_type' => Phone::TYPE_REALTOR]);
        }

        $marketEstateLinkPrice = new MarketEstateLinkPrice();
        $marketEstateLinkPrice->market_estate_link_id = $marketEstateLink->id;
        $marketEstateLinkPrice->created_at = $marketEstateLink->updated_at;
        $marketEstateLinkPrice->price = $parser->price;
        $marketEstateLinkPrice->price_kind = $parser->price_kind;
        $marketEstateLinkPrice->price_currency_id = $parser->price_currency_id;
        $marketEstateLinkPrice->save();

//        $marketEstateLinkView = new MarketEstateLinkView();
//        $marketEstateLinkView->market_estate_link_id = $marketEstateLink->id;
//        $marketEstateLinkView->created_at = time();
//        $marketEstateLinkView->count_views = $marketEstateLink->count_views;
//        $marketEstateLinkView->save();

        $marketEstateLinks = MarketEstateLink::findAll(['market_estate_id' => $marketEstate->id]);
        $countViews = 0;
        foreach ($marketEstateLinks as $marketEstateLink) {
            $countViews += $marketEstateLink->count_views;
        }

        $updateAttrs = [
            'has_realtor_phone' => (integer) ($marketEstateLink->contact_type == 'realtor' ? 1 : $marketEstate->has_realtor_phone),
            'has_owner_phone' => (integer) ($marketEstateLink->contact_type == 'owner' ? 1 : $marketEstate->has_owner_phone),
            'count_images' => $countImages,
            'images_from' => $parser->parsing_portal_id,
            'count_links' => count($marketEstateLinks),
            'count_views' => $countViews,
        ];
        if ($preview) {
            $updateAttrs['preview'] = $preview;
        }
        $marketEstate->setAttributes($updateAttrs);
        $marketEstate->save();

        if ($link) {
            $link->updateAttributes(['market_estate_link_id' => $marketEstateLink->id, 'thread' => null]);
        }


        $totalTime = round(microtime(true) - $parser->startTime(), 3);
        if ($totalTime > 10) {

            $resultOutput = PHP_EOL . "+({$parser->parsing_portal_id} ";
            $resultOutput .= 'Summ: ' . round(array_sum($parser->profilerData()), 3) . ', ';
            $resultOutput .= 'Total: ' . round(microtime(true) - $parser->startTime(), 3) . ', ';
            foreach ($parser->profilerData() as $method => $profilerDatum) {
                if ($profilerDatum > 0.05) {
                    $resultOutput .= " {$method} = {$profilerDatum} s, ";
                }
            }
            $resultOutput .= ')' . PHP_EOL . $parser->url . PHP_EOL;
            output($resultOutput);
        } else {
            stdout("+({$parser->parsing_portal_id})");
        }

        return true;
    }

    /**
     * @param null|string $parsingPortalId @see ParsingPortal constants
     * @param string $status @see MarketEstateLink::STATUS_ACTIVE, MarketEstateLink::STATUS_CLOSED and MarketEstateLink::STATUS_ERROR
     */
    public function update($parsingPortalId = null, $status = MarketEstateLink::STATUS_ACTIVE)
    {
        set_time_limit(0);
        ini_set('memory_limit', '512M');

        $this->output('closed', ['style' => 'color:#00c031;', 'title' => Html::encode('closed')], "\e[33m");
        $this->output("\n");

        $toUpdate = MarketEstateLink::find();
        $toUpdate->where('[[status]]=:status', [':status' => $status]);
        $toUpdate->andWhere('[[next_check_at]]<:next_check_at', [':next_check_at' => time()]);
        $toUpdate->andFilterWhere(['parsing_portal_id' => $parsingPortalId]);
        $toUpdate->limit(1000);

        $itemsUpdated = 0;
        $startTime = time();
        /** @var MarketEstateLink $marketEstateLink */
        foreach ($toUpdate->each() as $marketEstateLink) {
            if ($this->updateOne($marketEstateLink)) {
                $itemsUpdated++;
            }

            if ($itemsUpdated%1000 === 0) {
                $duration = gmdate('H:i:s', (time() - $startTime));

                $this->output("\n");
                $this->output("Обновлено {$itemsUpdated} за {$duration}", ['style' => 'color:blue;'], "\e[34;1m");
                $this->output("\n");
            }
        }

        $duration = gmdate('H:i:s', (time() - $startTime));

        $this->output("\n");
        $this->output("\n");
        $this->output("Всего обновлено {$itemsUpdated} за {$duration}", ['style' => 'color:blue;'], "\e[34;1m");
        $this->output("\n");
    }

    /**
     * @param MarketEstateLink $marketEstateLink
     * @param bool $updateMarketEstate
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function updateOne(MarketEstateLink $marketEstateLink, $updateMarketEstate = true)
    {
        /** @var Transaction $transaction */
        $transaction = \Yii::$app->db->beginTransaction();
        output("{$marketEstateLink->id}: {$marketEstateLink->url}\n");
        try {
            $parser = self::getObjectParser($marketEstateLink->url);

            /**
             * TODO: вполне возможно, что лучше возвращать сразу статус объекта.
             */
//            var_dump($parser->isClosed);
//            exit;
            if ($parser->isClosed) {
                $marketEstateLink->updateAttributes(['status' => MarketEstateLink::STATUS_CLOSED, 'checked_at' => time(), 'next_check_at' => time() + 60*60*24*90]);
                $this->output($marketEstateLink->id, ['style' => 'color:#00c031;', 'title' => Html::encode('closed')], "\e[33m");
                $transaction->commit();
                return true;
            }

            if (!$parser->price) {
                $marketEstateLink->updateAttributes(['status' => MarketEstateLink::STATUS_ERROR, 'checked_at' => time(), 'next_check_at' => time() + 60*60*24*90]);
                $this->output('#', ['style' => 'color:#red;', 'title' => Html::encode('closed')], "\e[31m");
                $transaction->commit();
                return true;
            }

            if ($parser->response->isRedirect()) {
                $newMarketEstateLink = MarketEstateLink::findOne(['url' => $parser->newUrl]);
                if ($newMarketEstateLink) {
                    MarketEstateLinkPhone::deleteAll(['market_estate_link_id' => $marketEstateLink->id]);
                    foreach ($marketEstateLink->marketEstateLinkViews as $marketEstateLinkView) {
                        $marketEstateLinkView->updateAttributes(['market_estate_link_id' => $newMarketEstateLink->id]);
                    }
                    foreach ($marketEstateLink->marketEstateLinkPrices as $marketEstateLinkPrice) {
                        $marketEstateLinkPrice->updateAttributes(['market_estate_link_id' => $newMarketEstateLink->id]);
                    }
                    $marketEstateLink->delete();
                } else {
                    $marketEstateLink->updateAttributes(['url' => $parser->newUrl]);
                }
                $transaction->commit();

                $this->updateOne($marketEstateLink);
                echo 'redirect: ' . $marketEstateLink->url;
                echo "\n";
                return true;
            }

            $marketEstateLink->checked_at = time();
            $marketEstateLink->next_check_at = $marketEstateLink->checked_at + 60*60*24;

            $marketEstateLink->count_views = $parser->portal_views;

            if ($marketEstateLink->updated_at != $parser->portal_updated_at) {
                $marketEstateLink->updated_at = $parser->portal_updated_at;
                $marketEstateLink->next_check_at = $marketEstateLink->checked_at + 60*60*24*3;
            } elseif ($marketEstateLink->updated_at < time()-60*60*24*15) {
                $marketEstateLink->next_check_at = $marketEstateLink->checked_at + 60*60*24*15;
            } elseif ($marketEstateLink->updated_at < time()-60*60*24*30) {
                $marketEstateLink->next_check_at = $marketEstateLink->checked_at + 60*60*24*30;
            }

            if ($marketEstateLink->price !== $parser->price || $marketEstateLink->price_currency_id != $parser->price_currency_id || $marketEstateLink->price_kind != $parser->price_kind) {

                $marketEstateLink->next_check_at = $marketEstateLink->checked_at + 60*60*12;

                $marketEstateLink->price = $parser->price;
                $marketEstateLink->price_kind = $parser->price_kind;
                $marketEstateLink->price_currency_id = $parser->price_currency_id;

                $marketEstateLinkPrice = new MarketEstateLinkPrice();
                $marketEstateLinkPrice->market_estate_link_id = $marketEstateLink->id;
                $marketEstateLinkPrice->created_at = $marketEstateLink->updated_at ? $marketEstateLink->updated_at : $marketEstateLink->created_at;
                $marketEstateLinkPrice->price = $parser->price;
                $marketEstateLinkPrice->price_kind = $parser->price_kind;
                $marketEstateLinkPrice->price_currency_id = $parser->price_currency_id;
                if (!$marketEstateLinkPrice->save()) {
                    throw new Exception('update error: Parsing::update() $marketEstateLinkPrice->save() ' . VarDumper::dumpAsString($marketEstateLinkPrice->errors));
                }
            }

//            if ($marketEstateLink->count_views) {
//                $marketEstateLinkView = new MarketEstateLinkView();
//                $marketEstateLinkView->market_estate_link_id = $marketEstateLink->id;
//                $marketEstateLinkView->created_at = time();
//                $marketEstateLinkView->count_views = $marketEstateLink->count_views;
//                if (!$marketEstateLinkView->save()) {
//                    throw new Exception('update error: Parsing::update() $marketEstateLinkView->save() ' . VarDumper::dumpAsString($marketEstateLinkView->errors));
//                }
//            }

            if (!$marketEstateLink->marketEstate->images) {
                $countImages = 0;
                foreach ($parser->images as $img) {
                    if (!(StringHelper::endsWith($img, '.jpg') || StringHelper::endsWith($img, '.jpeg') || StringHelper::endsWith($img, '.png'))) {
                        continue;
                    }

                    $marketEstateImage = new MarketEstateImage();
                    $marketEstateImage->source_url = $img;
                    $marketEstateImage->market_estate_id = $marketEstateLink->market_estate_id;
                    $marketEstateImage->status = MarketEstateImage::STATUS_NEW;
                    $marketEstateImage->save();

                    if ($marketEstateImage->status == MarketEstateImage::STATUS_SUCCESS)
                        $countImages++;
                }
                $marketEstateLink->count_images = $countImages;
                $marketEstateLink->marketEstate->setAttributes(['count_images' => $countImages, 'images_from' => $marketEstateLink->parsing_portal_id]);
                $marketEstateLink->marketEstate->save();
            }

            if (!$marketEstateLink->save()) {
                throw new Exception('update error: Parsing::update() $marketEstateLink->save() ' . VarDumper::dumpAsString($marketEstateLink->errors));
            }

            // update MarketEstate data
            if ($updateMarketEstate) {
                $marketEstate = $marketEstateLink->marketEstate;

                $marketEstate->is_new_building = $parser->is_new_building;
                $marketEstate->room_count = $parser->room_count;
                $marketEstate->area_total = $parser->area_total;
                $marketEstate->area_living = $parser->area_living;
                $marketEstate->area_kitchen = $parser->area_kitchen;
                $marketEstate->area_land = $parser->area_land;
                $marketEstate->floor = $parser->floor;
                $marketEstate->total_floors = $parser->total_floors;
                $marketEstate->layout = $parser->layout;
                $marketEstate->wall_material = $parser->wall_material;
                $marketEstate->building_type = $parser->building_type;
                $marketEstate->city_id = $parser->city_id;
                $marketEstate->district_id = $parser->district_id;
                $marketEstate->street_id = $parser->street_id;
                $marketEstate->street_name = $parser->street_name;
                $marketEstate->location_id = $parser->location_id;
                $marketEstate->map_lat = $parser->map_lat;
                $marketEstate->map_lng = $parser->map_lng;
                $marketEstate->house_num = $parser->house_num;

                if ($marketEstateLink->price_kind == 'per_object') {
                    $marketEstate->price_per_object = $marketEstateLink->price;
                    if ($marketEstate->area_total) {
                        $marketEstate->price_per_sqr = $marketEstateLink->price / $marketEstate->area_total;
                    }
                } else {
                    $marketEstate->price_per_object = $marketEstate->area_total ? $marketEstateLink->price * $marketEstate->area_total : $marketEstateLink->price;
                    $marketEstate->price_per_sqr = $marketEstateLink->price;
                }
                $marketEstate->price_currency_id = $marketEstateLink->price_currency_id;

                $marketEstate->save();

//                $isPhonesFromOwner = true;
//                $phoneIds = [];
//                foreach ($parser->phones as $phone) {
//                    /** @var Phone $phone */
//                    $phone = Phone::findOrCreate($phone, $parser->phoneType);
//                    if ($isPhonesFromOwner)
//                        $isPhonesFromOwner = $phone->isOwner();
//
//                    if (!MarketEstateLinkPhone::find()->where(['market_estate_link_id' => $marketEstateLink->id, 'phone_id' => $phone->id])->exists()) {
//                        $marketEstateLinkPhone = new MarketEstateLinkPhone();
//                        $marketEstateLinkPhone->market_estate_link_id = $marketEstateLink->id;
//                        $marketEstateLinkPhone->market_estate_id = $marketEstate->id;
//                        $marketEstateLinkPhone->phone_id = $phone->id;
//                        $marketEstateLinkPhone->save();
//                        $phoneIds[] = $marketEstateLinkPhone->phone_id;
//                    }
//                }
//                if (!empty($phoneIds)) {
//                    MarketEstateLinkPhone::deleteAll(['AND', ['market_estate_link_id' => $marketEstateLink->id], ['NOT IN', 'phone_id', $phoneIds]]);
//                }

//                $contactTypes = explode(',', $marketEstate->contact_type);
//                if (!in_array($marketEstateLink->contact_type, $contactTypes)) {
//                    $contactTypes[] = $marketEstateLink->contact_type;
//                }

//                $marketEstateLink->updateAttributes(['contact_type' => ($isPhonesFromOwner ? Phone::TYPE_OWNER : Phone::TYPE_REALTOR)]);
            }
            // end


            $transaction->commit();

//            echo 'OK: ' . $marketEstateLink->url;
//            echo "\n---------------------------------------------------------------\n";

            return true;

        } catch (Exception $e) {
            $transaction->rollBack();
            $this->output('#' . $e->getMessage(), ['style' => 'color:red;', 'title' => Html::encode($e->getMessage())], "\e[31m");
            ParsingError::create($marketEstateLink->url, 'update: ' . $e->getMessage());

            return false;
        }

    }
}
