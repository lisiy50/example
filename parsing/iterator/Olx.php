<?php
namespace app\parsing\iterator;

use app\parsing\parser\BaseListObject;
use app\parsing\parser\OlxPage;

class Olx extends Base
{
    /**
     * @return float|int
     */
    public function getTotalPages()
    {
        if($this->_totalPages === null) {
            $parser = new OlxPage();
            $this->_totalPages = $parser->getTotalPages($this->_url);
        }
        return $this->_totalPages;
    }

    /**
     * Fetches the next batch of data.
     * @return BaseListObject[] the data fetched
     */
    protected function fetchData()
    {
        $result = [];
        $page = $this->_page;
        $this->_page++;

        if($page > 100 || $page > $this->getTotalPages()) {
            return $result;
        }

        $parser = new OlxPage();
        $parser->setUrl($this->_url);

        $result = $parser->getObjectList($this->_url, $page);
        return $result;
    }
}
