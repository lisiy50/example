<?php

namespace app\parsing\connection;

use app\models\ParsingPortal;
use app\models\Proxy;
use DiDom\Document;
use yii\base\Exception;
use Zend\Http\Client;

/**
 * Class Olx
 * @package app\connection
 */
class Olx extends Base
{
    public $useProxy = true;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function getParsing_portal_id()
    {
        return ParsingPortal::OLX;
    }

    public function validBody($document)
    {
        if (!($document instanceof Document)) {
            $document = new Document($document);
        }
        if($document->has('#headerLogo')) {
            return true;
        }
        return false;
    }

    /**
     * @param string|null $uri
     * @param bool $refresh
     * @param array $options
     * @return null|\Zend\Http\Client
     */
    public function getClient($uri = null, $refresh = false, array $options = [
        'timeout' => 20,
        'sslverifypeer' => false,
        'adapter' => 'Zend\Http\Client\Adapter\Curl',
    ])
    {
        return parent::getClient($uri, $refresh, $options);
    }



    public function getResponse()
    {
        if (!$this->_response) {
            $client = $this->getClient($this->getUrl());
            $client->setOptions(['maxredirects' => 0]);
            $this->_response = $this->trySend($client);
        }
        return $this->_response;
    }

    /**
     * @param Client $client
     * @param int $limit
     * @return bool
     * @throws Exception
     */
    protected function applyProxy(Client $client, $limit = 5)
    {
        if($limit == 0) {
            output("{$this->url} Не удалось выбрать успешный прокси сервер");
            throw new Exception('Не удалось выбрать успешный прокси сервер');
        }

        /** @var Proxy $proxy */
        $proxy = $this->parsingPortal->randProxy;
        $proxy->applyTo($client);

//        echo "[proxy{$proxy->id}]";

        try {
            $client->setUri($this->url);
            $response = $client->send();
            var_dump($response->getBody());
            exit;
            if($this->validBody($response->getBody())) {
                return true;
            }
        } catch (Exception $exception) {
            echo $client->getUri();
            echo "\n";
            echo $exception->getMessage();
            echo "\n--------------------------------------\n";

            // просто ловим ошибку
        }
        return $this->applyProxy($client, $limit - 1);
    }
}
