<?php

namespace app\parsing\connection;

use app\models\ParsingPortal;
use app\models\Proxy;
use DiDom\Document;
use yii\base\BaseObject;
use yii\base\Exception;
use Zend\Http\Client;
use Zend\Http\Response;

/**
 * Class Base
 * @package app\connection
 *
 * @property string $parsing_portal_id @see self::getParsing_portal_id()
 * @property string $url @see self::getUrl()
 * @property array $headers @see self::getHeaders()
 * @property Response $response @see self::getResponse()
 *
 * @property ParsingPortal $parsingPortal
 */

abstract class Base extends BaseObject
{
    /** @var bool */
    public $useProxy = false;

    /** @var string */
    private $_url;

    /** @var array */
    private $_headers = [
        'Accept' => 'application/json, text/javascript, */*; q=0.01',
        'Accept-Encoding' => 'gzip, deflate, sdch',
        'Accept-Language' => 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,be;q=0.2,mk;q=0.2,uk;q=0.2',
        'Connection' => 'keep-alive',
        'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36',
    ];

    /** @var string */
    private $_content;

    /** @var Response */
    protected $_response;

    /** @var Client */
    protected $_client;

    /**
     * @return string
     */
    public abstract function getParsing_portal_id();

    /**
     * @param string|Document $document
     * @return bool
     */
    public abstract function validBody($document);

    public function getContent() {
        if (!$this->_content) {
            $this->_content = $this->response->getBody();
        }
        return $this->_content;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * @param string $val
     */
    public function setUrl(string $val)
    {
        $this->_url = $val;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * @param array $val
     */
    public function setHeaders(array $val)
    {
        $this->_headers = $val;
    }

    /**
     * @param string|null $uri
     * @param boolean $refresh
     * @param array $options
     * @return null|Client
     * @throws Exception
     */
    public function getClient($uri = null, $refresh = false, array $options = ['timeout' => 20,'sslverifypeer' => false,])
    {
        if($refresh) {
            $this->_client = null;
        }
        if($this->_client === null) {
            $this->_client = new Client($uri, $options);
            $this->_client->setAdapter((new Client\Adapter\Curl()));
            if($this->useProxy) {
                $this->applyProxy($this->_client);
            }
        }
        $this->_client->resetParameters();
        if($uri) {
            $this->_client->setUri($uri);
        }
        $this->_client->setHeaders($this->headers);
        return $this->_client;
    }

    /**
     * @param Client $client
     * @param int $limit
     * @return bool
     * @throws Exception
     */
    protected function applyProxy(Client $client, $limit = 5)
    {
        if($limit == 0) {
            output("{$this->url} Не удалось выбрать успешный прокси сервер");
            throw new Exception('Не удалось выбрать успешный прокси сервер');
        }

        /** @var Proxy $proxy */
        $proxy = $this->parsingPortal->randProxy;
        $proxy->applyTo($client);

//        echo "[proxy{$proxy->id}]";

        try {
            $client->setUri($this->url);
            $response = $client->send();
            if($this->validBody($response->getBody())) {
                return true;
            }
        } catch (Exception $exception) {
            echo $client->getUri();
            echo "\n";
            echo $exception->getMessage();
            echo "\n--------------------------------------\n";

            // просто ловим ошибку
        }
        return $this->applyProxy($client, $limit - 1);
    }

    /**
     * @param Client $client
     * @param int $limit
     * @return Response
     * @throws Exception
     */
    public function trySend(Client $client, $limit = 5)
    {
        if($limit == 0) {
            throw new Exception('Не удалось выполнить запрос');
        }
        try {
            $response = $client->send();
        } catch (Client\Adapter\Exception\TimeoutException $exception) {
            $response = $this->trySend($client, $limit - 1);
        }
        if ($response->isRedirect()) {
            // здесь нужно акуратно, потому что бывает, что редиректит не на объект, а куда то еще, и выходит, что ссылка меняется на неадекватную
            $this->setUrl($client->getUri()->getScheme() . '://' . $client->getUri()->getHost() . $client->getUri()->getPath());
            $client->setUri($this->getUrl());
            $response = $this->trySend($client, $limit - 1);
        }
        return $response;
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function getResponse()
    {
        if (!$this->_response) {
            $client = $this->getClient($this->url);
            $client->setOptions(['maxredirects' => 0]);
            $client->setHeaders($this->headers);
            $this->_response = $this->trySend($client);
        }
        return $this->_response;
    }

    /**
     * @return ParsingPortal
     * @throws \yii\base\Exception
     */
    public function getParsingPortal()
    {
        $parsingPortal = ParsingPortal::findOne($this->parsing_portal_id);
        if (!$parsingPortal) {
            throw new \yii\base\Exception('Необходимо указать parsing_portal_id');
        }
        return $parsingPortal;
    }
}
