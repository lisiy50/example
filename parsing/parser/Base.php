<?php

namespace app\parsing\parser;

use app\parsing\connection\Besplatka;
use app\parsing\connection\DomRia;
use app\models\ParsingPortal;
use app\parsing\connection\Fn;
use app\parsing\connection\Olx;
use app\parsing\connection\Realty100;
use app\parsing\connection\ReLviv;
use app\parsing\connection\Rieltor;
use DiDom\Document;
use yii\base\ErrorException;
use Zend\Http\Response;

/**
 * Class Base
 * @package app\parser
 *
 * @property string $url @see self::getUrl()
 * @property \app\parsing\connection\Base $connection @see self::getConnection
 * @property Response $response @see self::getResponse
 * @property string $content @see self::getContent
 * @property string $parsing_portal_id
 * @property Document $document
 */
abstract class Base extends \yii\base\BaseObject
{
    /** @var string */
    private $_url;

    /** @var \app\parsing\connection\Base */
    private $_connection;

    /** @var Response */
    private $_response;

    /** @var string */
    protected $_content;

    /** @var Document */
    protected $_document;

    /** @var string */
    protected $_newUrl;

//    ========= для профилирования
    private $_startTime;
    private $_profilerData = [];
    public function __construct(array $config = [])
    {
        $this->_startTime = microtime(true);

        parent::__construct($config);
    }
    public function __get($name)
    {
        $startTime = microtime(true);

        $result = parent::__get($name);

        $this->_profilerData[$name] = round(microtime(true) - $startTime, 5);
        return $result;
    }
    function profilerData()
    {
        return $this->_profilerData;
    }
    function startTime()
    {
        return $this->_startTime;
    }
//    ========= END для профилирования

    /**
     * @return string
     */
    public abstract function getParsing_portal_id();

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * @param string $val
     */
    public function setUrl(string $val)
    {
        $this->_url = $val;
    }

    /**
     * @return \app\parsing\connection\Base
     * @throws ErrorException
     */
    public function getConnection()
    {
        if (!$this->_connection) {
            switch ($this->getParsing_portal_id()) {
                case ParsingPortal::DOM_RIA:
                    $this->_connection = new DomRia(['url' => $this->url]);
                    break;
                case ParsingPortal::FN:
                    $this->_connection = new Fn(['url' => $this->url]);
                    break;
                case ParsingPortal::REALTY100:
                    $this->_connection = new Realty100(['url' => $this->url]);
                    break;
                case ParsingPortal::RIELTOR:
                    $this->_connection = new Rieltor(['url' => $this->url]);
                    break;
                case ParsingPortal::OLX:
                    $this->_connection = new Olx(['url' => $this->url]);
                    break;
                case ParsingPortal::RE_LVIV:
                    $this->_connection = new ReLviv(['url' => $this->url]);
                    break;
                case ParsingPortal::BESPLATKA:
                    $this->_connection = new Besplatka(['url' => $this->url]);
                    break;
            }
        }
        return $this->_connection;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        if (!$this->_response) {
            $this->_response = $this->connection->response;
            $this->setUrl($this->connection->url);
        }
        return $this->_response;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        if (!$this->_content) {
            $this->_content = $this->response->getBody();
        }
        return $this->_content;
    }

    /**
     * @return Document
     */
    public function getDocument()
    {
        if (!$this->_document) {
            $this->_document = new Document($this->content);
        }
        return $this->_document;
    }

    /**
     * @return string
     */
    public function getNewUrl()
    {
        return $this->_newUrl;
    }

    /**
     * @deprecated
     * Удаляет муссор из ссылок
     * @param string $url
     * @return string
     */
    public function prepareUrl(string $url)
    {
        $temp = parse_url($url);
        $url = $temp['scheme'] . '://';
        $url .= $temp['host'];
        $url .= $temp['path'];
        return $url;
    }

    /**
     * @param string $string
     * @param string|array $words
     * @return bool
     */
    protected function contain($string, $words)
    {
        $words = is_array($words) ? $words : [$words];
        foreach($words as $word) {
            if(mb_strpos($string, $word, null, 'utf-8') !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $number
     * @return int
     */
    protected function toInt($number)
    {
        return intval(preg_replace('#[^\d]#u', '', $number));
    }

    /**
     * @param $number
     * @return float
     */
    protected function toNumber($number)
    {
        $number = str_replace(',', '.', $number);
        return floatval(trim(preg_replace('#[^\d\.]#u', '', $number), '.'));
    }
}
