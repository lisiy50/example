<?php

namespace app\parsing\parser;

use app\models\ParsingPortal;
use DiDom\Document;
use DiDom\Element;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * Class OlxPage
 * @package app\parser
 */
class OlxPage extends BasePage
{
    /** @var int */
    public $limit = 100;

    /**
     * @return string
     */
    public function getParsing_portal_id()
    {
        return ParsingPortal::OLX;
    }

    /**
     * @param string|Document $document
     * @return bool
     */
    protected function validBody($document)
    {
        if (!($document instanceof Document)) {
            $document = new Document($document);
        }
        if($document->has('#headerLogo')) {
            return true;
        }
        return false;
    }

    /**
     * @param string $url
     * @param int $page
     * @return string
     */
    public function getPageUrl(string $url, int $page)
    {
        if ($page <= 1) {
            $pos = strpos($url, '?');
            if ($pos) {
                return substr($url, 0, $pos);
            } else {
                return $url;
            }
        } else {
            $query = parse_url($url, PHP_URL_QUERY);
            parse_str($query, $params);
            $params = ArrayHelper::merge($params, [
                'page' => $page,
            ]);
            $query = http_build_query($params);
            $pos = strpos($url, '?');
            if($pos === false) {
                return $url . '?' . $query;
            } else {
                return substr($url, 0, $pos) . '?' . $query;
            }
        }
    }

    /**
     * @param string $url
     * @return int
     */
    public function getTotalPages(string $url)
    {
        $url = $this->getPageUrl($url, 1);
        $this->setUrl($url);

        if ($this->document->has('#body-container div.pager .next')) {
            $this->document->first('#body-container div.pager .next')->remove();
            $items = $this->document->find('#body-container div.pager .item');
            $last = end($items);
            $totalPages = $this->toInt($last->first('span')->text());

            unset($items, $last);
        } else {
            $totalPages = 1;
        }

        return max($totalPages, 1);
    }

    /**
     * @param string $url
     * @param int $page
     * @return BaseListObject[]
     * @throws Exception
     */
    public function getObjectList(string $url, int $page)
    {
        $result = [];
        $url = $this->getPageUrl($url, $page);

        $client = $this->connection->getClient($url, true);
        $html = $this->connection->trySend($client)->getBody();

        $document = new Document($html);
        if(!$this->validBody($document)) {
            throw new Exception('Не валидный результат HTML');
        }
        /** @var Element $item */
        foreach($document->find('td.offer') as $item) {
            $result[] = new OlxListObject($item->html());
        }

        return $result;
    }

    /**
     * @deprecated
     * @param string $url
     * @param int $page
     * @return array
     * @throws Exception
     */
    public function getUrlList(string $url, int $page)
    {
        $result = [];

        $url = $this->getPageUrl($url, $page);
        $this->setUrl($url);

        $client = $this->connection->getClient($url, true);
        $response = $this->connection->trySend($client);

//        echo "\n";
//        echo $url . ': ' . $response->getStatusCode();
//        echo "\n";

        $html = $response->getBody();
        $document = new Document($html);
        if(!$this->validBody($document)) {
            throw new Exception('Не валидный результат HTML');
        }
        foreach($document->find('.offers .offer') as $offer) {
            $url = trim($offer->first('.detailsLink')->attr('href'));
            if($pos = strpos($url, '#')) {
                $url = substr($url, 0, $pos);
            }
            $tempUrl = $this->prepareUrl($url);
            if ($tempUrl)
                $result[] = $tempUrl;
        }

        return $result;
    }
}
