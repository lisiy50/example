<?php

namespace app\parsing\parser;

use app\models\ParsingPortal;

/**
 * Class OlxListObject
 * @package app\parser
 *
 */
class OlxListObject extends BaseListObject
{
    /**
     * @return string
     */
    public function getParsing_portal_id()
    {
        return ParsingPortal::OLX;
    }

    /**
     * Удаляет муссор из ссылок
     * @param string $url
     * @return string
     */
    public function prepareUrl(string $url)
    {
        $temp = parse_url($url);
        $url = $temp['scheme'];
        $url .= '://';
        $url .= $temp['host'];
        $url .= $temp['path'];
        return $url;
    }

    /**
     * новый ИД нужно искать в самом объекте
     * @return null|string
     */
    public function findObject_key()
    {
        return $this->document->first('table.fixed')->attr('data-id');
    }

    /**
     * ID объекта ищется в ссылке, но на ОЛКС этот ИД может повторятся
     * @return string
     */
    function getOldObjectKey()
    {
        if (preg_match('#-([^-]+)\.html$#', $this->url, $matches)) {
            return $matches[1];
        }
        return null;
    }


    /**
     * @return bool
     */
    public function getIsValid()
    {
        return $this->document->has('.price strong') && $this->document->has('table.fixed');
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        if (!$this->_url) {
            $this->_url = $this->prepareUrl($this->document->first('h3 a')->attr('href'));
        }
        return $this->_url;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return null;
    }

    /**
     * @return int
     */
    public function getPrice_currency_id()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getPrice_kind()
    {
        return null;
    }

    /**
     * @return int
     */
    public function getCreated_at()
    {
        return null;
    }

    /**
     * @return int
     */
    public function getUpdated_at()
    {
        return strtotime(date('Y-m-d'));
    }
}
