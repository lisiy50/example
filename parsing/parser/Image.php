<?php

namespace app\parsing\parser;

use app\models\Proxy;
use Yii;
use yii\helpers\FileHelper;
use Zend\Http\Client;

/**
 * Class Image
 * @package app\parser
 */
class Image
{
    public $useProxy = false;

    public function saveImage($image_url, $referer_url = null, $storage = 'amazon')
    {
        return $this->save($image_url, $referer_url, $storage);
    }

    public function save($image_url, $referer_url = null, $storage = 'amazon')
    {
        $file = new \SplFileInfo($image_url);
        $file->getExtension();

        $client = new Client($image_url, ['timeout' => 5,'sslverifypeer' => false,]);
        $client->setAdapter('Zend\Http\Client\Adapter\Curl');
        $headers = [
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Upgrade-Insecure-Requests' => '1',
        ];
        if ($referer_url) {
            $headers['Referer'] = $referer_url;
        }
        $client->setHeaders($headers);

        if ($this->useProxy) {
            $proxy = Proxy::rand();
            $proxy->applyTo($client);
        }

        $response = $client->send();

        if (!($response->getStatusCode() == 200 || $response->getStatusCode() == 304)) {
            return false;
        }

        if ($storage == 'amazon') {
            $path = date('Y') . '/' . date('m') . '/' . md5($image_url) . '.' . $file->getExtension();
            return Yii::$app->get('amazons3')->putImage($response->getBody(), $path);
        } elseif ($storage == 'local') {
            $subPath = date('Y') . '/' . date('m');
            $path = Yii::getAlias('@storage/' . $subPath);
            FileHelper::createDirectory($path);
            $name = md5($image_url) . '.' . $file->getExtension();
            if (!file_exists($path . '/' . $name))
                file_put_contents($path . '/' . $name, $response->getBody());
            return $subPath . '/' . $name;
        }
        return false;
    }
}
