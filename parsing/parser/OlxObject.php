<?php

namespace app\parsing\parser;

use app\models\Category;
use app\models\CityContext;
use app\models\Currency;
use app\models\DistrictContext;
use app\models\Location;
use app\models\MarketEstate;
use app\models\ParsingPortal;
use app\models\Phone;
use app\models\RealtyType;
use app\models\RegionContext;
use DiDom\Document;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * Class OlxObject
 * @package app\parser
 *
 * @property array $objectParams
 * @property string $contentPhone
 */
class OlxObject extends BaseObject
{
    /** @var array */
    private $_objectParams = [];
    /** @var null|string */
    protected $_contentPhone;

    /**
     * @return string
     */
    public function getParsing_portal_id ()
    {
        return ParsingPortal::OLX;
    }

    /**
     * @return null|string
     */
    public function findObject_key()
    {
        if (!$this->document->has('a.observe-link')) {
            var_dump($this->url);
            exit;
        }
        $classes = $this->document->first('a.observe-link')->attr('class');
        preg_match('#{id:(\d+)}#', $classes, $matches);
        return $matches[1];
    }

    /**
     * ID объекта ищется в ссылке, но на ОЛКС этот ИД может повторятся
     * @return string
     */
    function getOldObjectKey()
    {
        if (preg_match('#-([^-]+)\.html$#', $this->url, $matches)) {
            return $matches[1];
        }
        return null;
    }

    /**
     * @param string|Document $document
     * @return bool
     */
    protected function validBody($document)
    {
        if (!($document instanceof Document)) {
            $document = new Document($document);
        }
        if($document->has('#headerLogo')) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getObjectParams()
    {
        if (empty($this->_objectParams)) {
            foreach ($this->document->find('#offerdescription table.details table.item') as $itemTag) {
                $values = [];
                foreach ($itemTag->find('.value') as $valTag) {
                    if ($valTag->has('a')) {
                        foreach ($valTag->find('a') as $aTag) {
                            $values[] = trim($aTag->text());
                        }
                    } else {
                        $values[] = trim($valTag->text());
                    }
                }
                $value = implode(', ', $values);

                $label = trim($itemTag->first('th')->text());
                if (empty($label) or empty($value)) {
                    continue;
                }
                $this->_objectParams[$label] = preg_replace('#\s{2,}#u', ' ', $value);
            }
        }
        return $this->_objectParams;
    }

    /**
     * @param $label
     * @return null|string
     */
    public function getObjectParam($label)
    {
        if (array_key_exists($label, $this->objectParams)) {
            return $this->objectParams[$label];
        }
        return false;
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    public function getContentPhone()
    {
        if (!$this->_contentPhone) {

            if ($this->document->has('#contact_methods .link-phone')) {
                $subject = $this->document->first('#contact_methods .link-phone')->attr('class');
                preg_match("#{'path':'phone', 'id':'(.+)?',#", $subject, $matches);

                $tokenText = $this->document->first('#body-container script')->text();
                preg_match("#var phoneToken = '(.+?)';#", $tokenText, $matches2);


                if($matches && $matches2) {
                    $key = $matches[1];
                    $token = $matches2[1];
                    $host = parse_url($this->url, PHP_URL_HOST);
                    $client = $this->connection->getClient("https://{$host}/ajax/misc/contact/phone/{$key}/");
                    $client->setParameterGet(['pt' => $token]);
                    $client->setHeaders(ArrayHelper::merge($this->connection->headers, [
                        'Referer' => $this->url,
                        'X-Requested-With' => 'XMLHttpRequest',
                    ]));

                    $this->_contentPhone = $this->connection->trySend($client)->getBody();
                }
            }

        }
        return $this->_contentPhone;
    }

    /**
     * @return array
     */
    public function getPhones()
    {
        if (!$this->_phones) {
            if($json_phone = @json_decode($this->contentPhone, true)) {
                $phones = [];
                if(StringHelper::startsWith($json_phone['value'], '<span')) {
                    foreach(explode('span', $json_phone['value']) as $phoneString) {
                        $phoneString = preg_replace('#[^0-9]#', '', $phoneString);
                        if($phoneString) {
                            $phones[] = $phoneString;
                        }
                    }
                } else {
                    $phones = [preg_replace('#[^0-9]#', '', $json_phone['value'])];
                }
                foreach($phones as $key => $phone) {
                    if(mb_strlen($phone, 'utf-8') == 12 && StringHelper::startsWith($phone, '38')) {
                        $phones[$key] = mb_substr($phone, 2, null, 'utf-8');
                    }
                }
                $this->_phones = $phones;
            }
        }
        return $this->_phones;
    }

    /**
     * @return string
     */
    public function getPhoneType()
    {
        if (!$this->_phoneType) {
            switch ($this->getObjectParam('Объявление от')) {
                case 'Бизнес':
                    $this->_phoneType = Phone::TYPE_REALTOR;
                    break;
                case 'Частного лица':
                    $this->_phoneType = Phone::TYPE_OWNER;
                    break;
                default:
                    $this->_phoneType = parent::getPhoneType();
            }
        }
        return $this->_phoneType;
    }

    /**
     * @return string
     */
    public function getContact_name()
    {
        if (!$this->_contact_name)
            $this->_contact_name = trim($this->document->first('.offer-user__details h4')->text());
        return $this->_contact_name;
    }

    /**
     * @return string
     */
    public function getPriceText()
    {
        if (!$this->_priceText && $this->document->has('#offeractions .price-label strong')) {
            $this->_priceText = trim($this->document->first('#offeractions .price-label strong')->text());
        }
        return $this->_priceText;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        if (!$this->_price) {
            if (StringHelper::endsWith($this->priceText, 'грн.')) {
                $this->_price = $this->toInt(mb_substr($this->priceText, 0, -5, 'utf-8'));
            } else {
                $this->_price = $this->toInt(mb_substr($this->priceText, 0, -2, 'utf-8'));
            }
        }
        return $this->_price;
    }

    /**
     * @return int
     */
    public function getPrice_currency_id()
    {
        if (!$this->_price_currency_id) {
            if (StringHelper::endsWith($this->priceText, 'грн.')) {
                $this->_price_currency_id = Currency::CURRENCY_UAH;
            } else if(StringHelper::endsWith($this->priceText, '$')) {
                $this->_price_currency_id = Currency::CURRENCY_USD;
            } else if(StringHelper::endsWith($this->priceText, '€')) {
                $this->_price_currency_id = Currency::CURRENCY_EUR;
            }
        }
        return $this->_price_currency_id;
    }

    /**
     * @return string
     */
    public function getPrice_kind()
    {
        if (!$this->_price_kind)
            $this->_price_kind = 'per_object';
        return $this->_price_kind;
    }

    /**
     * @return string
     */
    public function getSystem_text()
    {
        if (!$this->_system_text)
            $this->_system_text = trim($this->document->first('#breadcrumbTop .middle')->text());
        return $this->_system_text;
    }

    /**
     * @return int
     */
    public function getDeal()
    {
        if (!$this->_deal) {
            if (strstr($this->system_text, 'посуточно')) {
                $this->_deal = MarketEstate::DEAL_RENT_DAY;
            } elseif (strstr($this->system_text, 'Аренда')) {
                $this->_deal = MarketEstate::DEAL_RENT;
            } else {
                $this->_deal = MarketEstate::DEAL_SELL;
            }
        }
        return $this->_deal;
    }

    /**
     * @return string
     */
    public function getAddress_text()
    {
        if (!$this->_address_text)
            $this->_address_text = trim($this->document->first('.offercontent .show-map-link')->text());
        return $this->_address_text;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function getRegion_id()
    {
        if (!$this->_region_id) {
            $addressArr = explode(',', $this->address_text);
            $this->_region_id = $this->getRegionId($addressArr[1]);
        }
        return $this->_region_id;
    }

    /**
     * @param $regionName
     * @return int
     * @throws Exception
     */
    public function getRegionId($regionName)
    {
        $regionId = null;
        if (mb_strpos($regionName, ' область') || mb_strpos($regionName, 'Крым') !== false) {
            $regionName = trim(str_replace(' область', '', $regionName));

            $regionId = RegionContext::findInContext($regionName);
            if (!$regionId) {
                throw new Exception("Нет совпадений по области \"{$regionName}\"");
            }
        } else {
            /** @var Location[] $locationModels */
//            $locationModels = Location::find()->where('type IN ("regional_center", "city") AND name LIKE :name', [':name' => trim($regionName)])->all();
            $locationModels = Location::find()->where(['type' => [Location::TYPE_REGIONAL_CENTER, Location::TYPE_CITY], 'name' => trim($regionName)])->all();

            if (count($locationModels) == 1) {
                $parent = $locationModels[0];
                while($parent->parent){
                    $parent = $parent->parent;
                }
                $regionId = $parent->id;
            } else {
                throw new Exception('Более одного совпадения по области1');
            }
        }
        return $regionId;
    }

    /**
     * район в области
     */
    public function getCityRegionId()
    {
        $regionId = null;
        $addressArr = explode(',', $this->address_text);
        if (isset($addressArr[2])) {
            $cityRegionFullName = trim($addressArr[2]);
        } else {
            return $regionId;
        }
        if (mb_strpos($cityRegionFullName, ' район')) {
            $cityRegionName = trim(str_replace(' район', '', $cityRegionFullName));
            /** @var Location[] $cityRegion */
            $regionId = Location::find()->select('id')
                ->where([
                    'AND',
                    ['type' => [Location::TYPE_OTHER, Location::TYPE_COUNTY]],
                    ['parent_id' => $this->region_id],
                    ['OR', ['name' => $cityRegionName], ['name' => $cityRegionFullName]],
                ])->scalar();

        }
        return $regionId;
    }

    /**
     * @return string
     */
    public function getCity_id()
    {
        if ($this->_city_id === false) {
            $cityRegionId = $this->getCityRegionId();
            if ($cityRegionId) {
                $addressArr = explode(',', $this->address_text);
                $city = Location::findOne(['parent_id' => $cityRegionId, 'name' => $addressArr[0]]);
                if ($city) {
                    $this->_city_id = $city->id;
                }
            }
            if ($this->_city_id === false) {
                $this->_city_id = CityContext::findInContext($this->region_id, $this->address_text);
            }
        }
        return $this->_city_id;
    }

    /**
     * @return integer район города, родитель для микрорайона
     */
    public function getBorough_id()
    {
        if ($this->_borough_id === false && $this->city_id) {
            $text = $this->document->first('.offer-titlebox__details .show-map-link')->text();
            $this->_borough_id = DistrictContext::findInContext($this->city_id, $text, DistrictContext::TYPE_BOROUGH);

        }
        return $this->_borough_id;
    }

    /**
     * @return null|int
     */
    public function getRealty_type()
    {
        if (!$this->_realty_type) {
            foreach ($this->getObjectParams() as $label=>$value) {
                switch ($label) {
                    case 'Тип аренды':
                        switch($value) {
                            case 'Комнаты посуточно':
                                $this->_realty_type = RealtyType::TYPE_1_02;
                                break;
                            case 'Долгосрочная аренда комнат':
                                $this->_realty_type = RealtyType::TYPE_1_02;
                                break;
                            case 'Койко-места':
                                $this->_realty_type = null; // не поддерживаем загрузку
                                break;
                            case 'Аренда ресторанов / баров':
                                $this->_realty_type = RealtyType::TYPE_2_14;
                                break;
                            case 'Аренда магазинов / салонов':
                                $this->_realty_type = RealtyType::TYPE_2_11;
                                break;
                            case 'Аренда складов':
                                $this->_realty_type = RealtyType::TYPE_2_12;
                                break;
                            case 'Аренда помещений промышленного назначения':
                                $this->_realty_type = RealtyType::TYPE_2_13;
                                break;
                            case 'Аренда отдельно стоящих зданий':
                            case 'Аренда баз отдыха':
                                $this->_realty_type = RealtyType::TYPE_2_16;
                                break;
                        }
                        break;
                    case 'Тип дома':
                        $this->_realty_type = RealtyType::TYPE_1_03;
                        break;
                    case 'Тип участка':
                        switch($value) {
                            case 'Земля под индивидуальное строительство':
                                $this->_realty_type = RealtyType::TYPE_3_20;
                                break;
                            case 'Земля промышленного назначения':
                                $this->_realty_type = RealtyType::TYPE_3_23;
                                break;
                            case 'Земля под сад / огород':
                                $this->_realty_type = RealtyType::TYPE_3_21;
                                break;
                            case 'Земля сельскохозяйственного назначения':
                                $this->_realty_type = RealtyType::TYPE_3_22;
                                break;
                        }
                        break;
                    case 'Вид помещения':
                        switch($value) {
                            case 'Продажа магазинов / салонов':
                                $this->_realty_type = RealtyType::TYPE_2_11;
                                break;
                            case 'Продажа офисов':
                                $this->_realty_type = RealtyType::TYPE_2_10;
                                break;
                            case 'Продажа баз отдыха':
                                $this->_realty_type = RealtyType::TYPE_2_16;
                                break;
                            case 'Продажа складов':
                                $this->_realty_type = RealtyType::TYPE_2_12;
                                break;
                            case 'Продажа помещений промышленного назначения':
                                $this->_realty_type = RealtyType::TYPE_2_13;
                                break;
                            case 'Продажа помещений свободного назначения':
                                $this->_realty_type = RealtyType::TYPE_2_16;
                                break;
                            case 'Продажа ресторанов / баров':
                                $this->_realty_type = RealtyType::TYPE_2_14;
                                break;
                            case 'Продажа отдельно стоящих зданий':
                                $this->_realty_type = RealtyType::TYPE_2_16;
                                break;
                        }
                        break;
                }
            }
        }
        if (!$this->_realty_type) {
            if (strstr($this->system_text, 'квартир')) {
                $this->_realty_type = RealtyType::TYPE_1_01;
            } elseif (strstr($this->system_text, 'комнат')) {
                $this->_realty_type = RealtyType::TYPE_1_02;
            } elseif (strstr($this->system_text, 'домов')) {
                $this->_realty_type = RealtyType::TYPE_1_03;
            } elseif (strstr($this->system_text, 'земли')) {
                $this->_realty_type = RealtyType::TYPE_3_20;
            } elseif (strstr($this->system_text, 'гаражей / стоянок')) {
                $this->_realty_type = RealtyType::TYPE_2_16;
            } elseif (strstr($this->system_text, 'помещений')) {
                $this->_realty_type = RealtyType::TYPE_2_16;
            }
        }
        return $this->_realty_type;
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        if (!$this->_category) {
            $realtyType = RealtyType::findOne(['id' => $this->realty_type]);
            if($realtyType) {
                $this->_category = $realtyType->category;
            }
        }
        return $this->_category;
    }

    /**
     * @return int
     */
    public function getPortal_views()
    {
        if (!$this->_portal_views) {
            $this->document->first('#offerbottombar .clr')->remove();
            $this->_portal_views = intval($this->document->first('#offerbottombar div strong')->text());
        }
        return $this->_portal_views;
    }

    /**
     * @return false|int
     */
    public function getPortal_created_at()
    {
        if (!$this->_portal_created_at) {
            $monthes = ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря',' '];
            $monthNums = ['01','02','03','04','05','06','07','08','09','10','11','12','.'];
            $addedArr = explode(',', $this->document->first('span.pdingleft10.brlefte5')->text());
            $addedDate = str_replace($monthes, $monthNums, trim($addedArr[1]));
            $this->_portal_created_at = strtotime($addedDate);
            if (!$this->_portal_created_at)
                $this->_portal_created_at = time();
        }
        return $this->_portal_created_at;
    }

    /**
     * @return false|int
     */
    public function getPortal_updated_at()
    {
        return $this->portal_created_at;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        if (empty($this->_images)) {
            foreach($this->document->find('#offerdescription .img-item') as $imgTag) {
                if ($imgTag->first('img'))
                    $this->_images[] = $imgTag->first('img')->attr('src');
            }
        }
        return $this->_images;
    }

    /**
     * @return int
     */
    public function getIs_new_building()
    {
        if (!$this->_is_new_building) {
            $this->_is_new_building = 0;
            foreach ($this->getObjectParams() as $label=>$value) {
                if ($label == 'Тип квартиры' && $value == 'Новостройки') {
                    $this->_is_new_building = 1;
                    break;
                }
            }
        }
        return $this->_is_new_building;
    }

    public function getMap_lat()
    {
        if (!$this->_map_lat && $this->document->has('#mapcontainer')) {
            $mapContainer = $this->document->first('#mapcontainer');
            if ($mapContainer->attr('data-rad') < 500) {
                $this->_map_lat = (float)$mapContainer->attr('data-lat');
                $this->_map_lng = (float)$mapContainer->attr('data-lon');
            }
        }
        if (!$this->_map_lat) {
            $this->_map_lat = $this->coordsFromOsm['map_lat'];
        }
        return $this->_map_lat;
    }

    public function getMap_lng()
    {
        if (!$this->_map_lng && $this->document->has('#mapcontainer')) {
            $mapContainer = $this->document->first('#mapcontainer');
            if ($mapContainer->attr('data-rad') < 500) {
                $this->_map_lat = (float)$mapContainer->attr('data-lat');
                $this->_map_lng = (float)$mapContainer->attr('data-lon');
            }
        }
        if (!$this->_map_lng) {
            $this->_map_lng = $this->coordsFromOsm['map_lng'];
        }
        return $this->_map_lng;
    }

    /**
     * @return int
     */
    public function getRoom_count()
    {
        if (!$this->_room_count) {
            foreach ($this->getObjectParams() as $label=>$value) {
                if ($label == 'Количество комнат') {
                    $this->_room_count = $this->toInt($value);
                    break;
                }
            }
            if ($this->_room_count == 0) {
                $this->_room_count = null;
            }
        }
        if($this->realty_type == RealtyType::TYPE_1_02 && !$this->_room_count) { // если тип комната
            $this->_room_count = 1;
        }
        return $this->_room_count;
    }

    /**
     * @return int
     */
    public function getArea_total()
    {
        if (!$this->_area_total) {
            foreach ($this->getObjectParams() as $label=>$value) {
                if ($this->category == Category::LAND && $label == 'Площадь') {
                    $this->_area_total = $this->formatArea(mb_substr($value, 0, -3, 'utf-8'));
                } elseif ($this->category == Category::COMMERCIAL && ($label == 'Площадь' || $label == 'Общая площадь')) {
                    $this->_area_total = $this->formatArea(mb_substr($value, 0, -3, 'utf-8'));
                } else {
                    if ($label == 'Общая площадь' || $label == 'Площадь дома') {
                        $this->_area_total = $this->formatArea(mb_substr($value, 0, -3, 'utf-8'));
                    } elseif ($label == 'Площадь') {
                        if (StringHelper::endsWith($value, 'м2')) {
                            $this->_area_total = $this->formatArea(mb_substr($value, 0, -3, 'utf-8'));
                        } elseif(StringHelper::endsWith($value, 'соток')) {
                            $this->_area_total = $this->formatArea(mb_substr($value, 0, -6, 'utf-8'));
                        } else {
                            $this->_area_total = $this->formatArea(mb_substr($value, 0, -3, 'utf-8')) * 100;
                        }
                        break;
                    }
                }

            }
            if (!$this->_area_total) {
                $this->_area_total = $this->findAreaInText($this->promo_text, 'total');
            }
        }
        if ($this->_area_total == 0) {
            $this->_area_total = null;
        }
        return $this->_area_total;
    }

    /**
     * @return bool|int
     */
    public function getArea_living()
    {
        if (!$this->_area_living) {
            foreach ($this->getObjectParams() as $label=>$value) {
                if ($label == 'Жилая площадь') {
                    $this->_area_living = $this->formatArea(mb_substr($value, 0, -3, 'utf-8'));
                    break;
                }
            }
            if (!$this->_area_living) {
                $this->_area_living =  $this->findAreaInText($this->promo_text, 'life');
            }
        }
        if ($this->_area_living == 0) {
            $this->_area_living = null;
        }
        return $this->_area_living;
    }

    /**
     * @return bool|int
     */
    public function getArea_kitchen()
    {
        if (!$this->_area_kitchen) {
            foreach ($this->getObjectParams() as $label=>$value) {
                if ($label == 'Площадь кухни') {
                    $this->_area_kitchen = $this->formatArea(mb_substr($value, 0, -3, 'utf-8'));
                    break;
                }
            }
            if (!$this->_area_kitchen) {
                $this->_area_kitchen = $this->findAreaInText($this->promo_text, 'kitchen');
            }
        }
        if ($this->_area_kitchen == 0) {
            $this->_area_kitchen = null;
        }
        return $this->_area_kitchen;
    }

    /**
     * @return int
     */
    public function getArea_land()
    {
        if (!$this->_area_land) {
            foreach ($this->getObjectParams() as $label=>$value) {
                if ($this->realty_type == RealtyType::TYPE_2_10 && $label == 'Площадь') {
                    if(StringHelper::endsWith($value, 'соток')) {
                        $this->_area_land = $this->formatArea(mb_substr($value, 0, -6, 'utf-8'));
                    } else {
                        $this->_area_land = $this->formatArea(mb_substr($value, 0, -3, 'utf-8')) * 100;
                    }
                    break;
                }
            }
        }
        if ($this->_area_land == 0) {
            $this->_area_land = null;
        }
        return $this->_area_land;
    }

    /**
     * @return int
     */
    public function getFloor()
    {
        if (!$this->_floor) {
            foreach ($this->getObjectParams() as $label=>$value) {
                if ($label == 'Этаж') {
                    $this->_floor = $this->toInt(preg_replace('#[^0-9]#', '', $value));
                    break;
                }
            }
        }
        if ($this->_floor == 0) {
            $this->_floor = null;
        }
        return $this->_floor;
    }

    /**
     * @return mixed
     */
    public function getTotal_floors()
    {
        if (!$this->_total_floors) {
            foreach ($this->getObjectParams() as $label=>$value) {
                if ($label == 'Этажность' || $label == 'Этажность дома') {
                    $this->_total_floors = $this->toInt(preg_replace('#[^0-9]#', '', $value));
                    break;
                }
            }
        }
        if ($this->_total_floors == 0) {
            $this->_total_floors = null;
        }
        return $this->_total_floors;
    }

    /**
     * @return string
     */
    public function getPromo_title()
    {
        if (!$this->_promo_title) {
            if ($toRemove = $this->document->first('.offerheadinner h1 span.label-promoted2'))
                $toRemove->remove();
            $this->_promo_title = trim($this->document->first('#offerdescription .offer-titlebox h1')->text());
        }
        return $this->_promo_title;
    }

    /**
     * @return string
     */
    public function getPromo_text()
    {
        if (!$this->_promo_text) {
            if ($toRemove = $this->document->first('#textContent .spoilerHidden'))
                $toRemove->remove();
            $this->_promo_text = trim($this->document->first('#textContent')->text());
        }
        return $this->_promo_text;
    }

    function getFeature_text()
    {
        if (!$this->_feature_text) {
            foreach ($this->objectParams as $label => $value) {
                $this->_feature_text .= "{$label}: {$value}" . PHP_EOL;
            }
            $this->_feature_text = trim($this->_feature_text);
        }
        return $this->_feature_text;
    }

    /**
     * @return bool
     */
    public function getIsClosed()
    {
        return $this->response->getStatusCode() != 200 || $this->response->isRedirect() || !$this->document->has('a.observe-link');
    }

    public function getLayout()
    {
        switch ($this->getObjectParam('Планировка')) {
            case 'Смежная, проходная':
                $this->_layout = MarketEstate::LAYOUT_ADJACENT;
                break;
            case 'Раздельная':
            case 'Двухсторонняя':
                $this->_layout = MarketEstate::LAYOUT_SEPARATE;
                break;
            case 'Студия':
            case 'Смарт-квартира':
                $this->_layout = MarketEstate::LAYOUT_STUDIO;
                break;
            case 'Пентхаус':
                $this->_layout = MarketEstate::LAYOUT_PENTHOUSE;
                break;
            case 'Многоуровневая':
                $this->_layout = MarketEstate::LAYOUT_MULTILEVEL;
                break;
            case 'Малосемейка, гостинка':
            case 'Свободная планировка':
        }
        return $this->_layout;
    }

    public function getWall_material()
    {
        if (!$this->_wall_material) {
            switch ($this->getObjectParam('Тип стен')) {
                case 'Кирпичный':
                    $this->_wall_material = MarketEstate::WALL_MATERIAL_BRICKS;
                    break;
                case 'Панельный':
                    $this->_wall_material = MarketEstate::WALL_MATERIAL_PANEL;
                    break;
                case 'Монолитный':
                    $this->_wall_material = MarketEstate::WALL_MATERIAL_MONOLITH;
                    break;
                case 'Деревянный':
                    $this->_wall_material = MarketEstate::WALL_MATERIAL_WOOD;
                    break;
                case 'Газоблок':
                    $this->_wall_material = MarketEstate::WALL_MATERIAL_GAS_BLOCK;
                    break;
            }
        }
        return $this->_wall_material;
    }

    public function getBuilding_type()
    {
        if (!$this->_building_type) {
            switch ($this->getObjectParam('Тип дома')) {
                case 'Царский дом':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_100;
                    break;
                case 'Сталинка':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_101;
                    break;
                case 'Хрущевка':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_102;
                    break;
                case 'Чешка':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_103;
                    break;
                case 'Гостинка':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_104;
                    break;
                case 'Совмин':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_105;
                    break;
                case 'Общежитие':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_106;
                    break;
                case 'Жилой фонд 80-90-е':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_107;
                    break;
                case 'Жилой фонд 91-2000-е':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_108;
                    break;
                case 'Жилой фонд 2001-2010-е':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_109;
                    break;
                case 'Жилой фонд от 2011 г.':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_110;
                    break;
                case 'На этапе строительства':
                    $this->_building_type = MarketEstate::BUILDING_TYPE_111;
                    break;
            }
        }
        return $this->_building_type;
    }

}
