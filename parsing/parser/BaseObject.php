<?php

namespace app\parsing\parser;

use app\helpers\ParsingTools;
use app\helpers\Polygon;
use app\models\DistrictContext;
use app\models\Location;
use app\models\Phone;
use app\models\StreetContext;
use Yii;
use app\models\Street;
use DiDom\Document;
use yii\base\ExitException;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use Zend\Http\Client;

/**
 * Class BaseObject
 * @package app\parser
 *
 *
 * Magic properties:
 * @property $category
 * @property $realty_type
 * @property $system_text
 * @property $promo_title
 * @property $promo_text
 * @property $feature_text
 * @property $phones
 * @property $contact_name
 * @property $price
 * @property $price_per_object
 * @property $price_per_sqr
 * @property $price_currency_id
 * @property $price_kind
 * @property $deal
 * @property $is_new_building
 * @property $room_count
 * @property $area_total
 * @property $area_living
 * @property $area_kitchen
 * @property $area_land
 * @property $floor
 * @property $total_floors
 * @property $layout
 * @property $wall_material
 * @property $building_type
 * @property $address_text
 * @property $location_id
 * @property $region_id
 * @property $city_id
 * @property $borough_id
 * @property $district_id
 * @property $street_id
 * @property $street_name
 * @property $house_num
 * @property double $map_lat
 * @property double $map_lng
 * @property array $images
 * @property $portal_created_at
 * @property $portal_updated_at
 * @property $portal_views
 *
 * @property string $priceText
 * @property Street $streetModel
 * @property string $phoneType
 * @property boolean $isClosed
 * @property string $object_key
 * @property array $coordsFromOsm
 */
abstract class BaseObject extends Base
{
    /** @var int */
    protected $_category;
    /** @var int */
    protected $_realty_type;
    /** @var string */
    protected $_system_text;
    /** @var string */
    protected $_promo_title;
    /** @var string */
    protected $_promo_text;
    /** @var string */
    protected $_feature_text;
    /** @var array[] */
    protected $_phones = [];
    /** @var string */
    protected $_phoneType;
    /** @var string */
    protected $_contact_name;
    /** @var string */
    protected $_priceText;
    /** @var int */
    protected $_price;
    /** @var int */
    protected $_price_currency_id;
    /** @var string */
    protected $_price_kind;
    /** @var int */
    private $_price_per_object;
    /** @var int */
    private $_price_per_sqr;
    /** @var int */
    protected $_deal;
    /** @var int */
    protected $_is_new_building = 0;
    /** @var int */
    protected $_room_count;
    /** @var int */
    protected $_area_total;
    /** @var float */
    protected $_area_living;
    /** @var float */
    protected $_area_kitchen;
    /** @var float */
    protected $_area_land;
    /** @var int */
    protected $_floor;
    /** @var int */
    protected $_total_floors;
    /** @var int */
    protected $_layout;
    /** @var int */
    protected $_wall_material;
    /** @var int */
    protected $_building_type;
    /** @var string */
    protected $_address_text;
    /** @var int */
    protected $_location_id;
    /** @var int */
    protected $_region_id;
    /** @var int */
    protected $_city_id = false;
    /** @var int */
    protected $_borough_id = false;
    /** @var int */
    protected $_district_id = false;
    /** @var int */
    protected $_street_id = false;
    /** @var double */
    protected $_map_lat;
    /** @var double */
    protected $_map_lng;
    /** @var string */
    protected $_house_num;
    /** @var array */
    protected $_images = [];
    /** @var int */
    protected $_portal_created_at;
    /** @var int */
    protected $_portal_updated_at;
    /** @var int */
    protected $_portal_views;
    /** @var boolean */
    protected $_isClosed;
    /** @var bool|null|Location */
    private $_cityModel = false;
    /** @var bool|null|string */
    private $_cityName = false;
    /** @var Street */
    protected $_streetModel;
    /** @var string */
    protected $_street_name;
    /** @var string */
    protected $_object_key;
    /** @var array */
    private $_areaFromTextCache = [];
    /** @var array */
    private $_floorFromTextCache = [];
    /** @var array */
    private $_coordsFromOsm;

    /**
     * @return int
     */
    public abstract function getCategory();
    /**
     * @return int
     */
    public abstract function getRealty_type();
    /**
     * @return string
     */
    public abstract function getSystem_text();
    /**
     * @return string
     */
    public abstract function getPromo_title();
    /**
     * @return string
     */
    public abstract function getPromo_text();
    /**
     * @return string
     */
    public abstract function getFeature_text();
    /**
     * @return string[]
     */
    public abstract function getPhones();
    /**
     * @return string
     */
    public abstract function getContact_name();
    /**
     * @return string
     */
    public abstract function getPriceText();
    /**
     * @return int
     */
    public abstract function getPrice();
    /**
     * @return int
     */
    public abstract function getPrice_currency_id();
    /**
     * @return string
     */
    public abstract function getPrice_kind();
    /**
     * @return int
     */
    public abstract function getDeal();
    /**
     * @return int
     */
    public abstract function getIs_new_building();
    /**
     * @return double
     */
    public abstract function getMap_lat();
    /**
     * @return double
     */
    public abstract function getMap_lng();
    /**
     * @return int
     */
    public abstract function getRoom_count();
    /**
     * @return float
     */
    public abstract function getArea_total();
    /**
     * @return float
     */
    public abstract function getArea_living();
    /**
     * @return float
     */
    public abstract function getArea_kitchen();
    /**
     * @return float
     */
    public abstract function getArea_land();
    /**
     * @return int
     */
    public abstract function getFloor();
    /**
     * @return int
     */
    public abstract function getTotal_floors();
    /**
     * @return int
     */
    public abstract function getLayout();
    /**
     * @return int
     */
    public abstract function getWall_material();
    /**
     * @return int
     */
    public abstract function getBuilding_type();
    /**
     * @return string
     */
    public abstract function getAddress_text();
    /**
     * @return int
     */
    public abstract function getRegion_id();
    /**
     * @return int
     */
    public abstract function getCity_id();
    /**
     * @return string[]
     */
    public abstract function getImages();
    /**
     * @return int
     */
    public abstract function getPortal_created_at();
    /**
     * @return int
     */
    public abstract function getPortal_updated_at();
    /**
     * @return int
     */
    public abstract function getPortal_views();
    /**
     * @return int
     */
    public abstract function getIsClosed();
    /**
     * @return integer район города, родитель для микрорайона
     */
    public abstract function getBorough_id();

    /**
     * @param string|Document $body
     * @return bool
     */
    protected abstract function validBody($body);

    /**
     * @return string
     */
    public abstract function findObject_key();

    /**
     * возвращает ключ объекта если задана ссылка на объект, если нет, то выбрасывает ексепшин
     * @return string
     * @throws ExitException
     */
    public function getObject_key()
    {
        if (!$this->_object_key) {
            if ($this->url) {
                $this->_object_key = $this->findObject_key();
            } else {
                throw new ExitException(0, 'Необходимо указать ссылку на объект');
            }
        }
        return $this->_object_key;
    }

    /**
     * @return string
     */
    public function getPhoneType()
    {
        if (!$this->_phoneType)
            $this->_phoneType = Phone::TYPE_NOT_KNOW;
        return $this->_phoneType;
    }

    /**
     * @return int
     */
    public function getPrice_per_object()
    {
        if (!$this->_price_per_object) {
            if ($this->price_kind == 'per_object') {
                $this->_price_per_object = $this->price;
            } else {
                $this->_price_per_object = round($this->price * $this->area_total);
            }
        }
        return $this->_price_per_object;
    }

    /**
     * @return int
     */
    public function getPrice_per_sqr()
    {
        if (!$this->_price_per_sqr) {
            if ($this->price_kind == 'per_object') {
                $this->_price_per_sqr = $this->area_total > 0 ? round($this->price / $this->area_total * 100) / 100 : 0;
            } else {
                $this->_price_per_sqr = $this->price;
            }
        }
        return $this->_price_per_sqr;
    }

    /**
     * @return int
     */
    public function getLocation_id()
    {
        if(!$this->city_id) {
            $this->_location_id = $this->region_id;
        } elseif ($this->district_id) {
            $this->_location_id = $this->district_id;
        } elseif ($this->borough_id) {
            $this->_location_id = $this->borough_id;
        } else {
            $this->_location_id = DistrictContext::findInContext($this->city_id, $this->system_text);
        }
        if(!$this->_location_id) {
            $this->_location_id = $this->city_id;
        }
        return $this->_location_id;
    }

    public function getCityModel()
    {
        if ($this->_cityModel === false) {
            $this->_cityModel = Location::findOne(['id' => $this->city_id]);
        }
        return $this->_cityModel;
    }

    public function getCityName()
    {
        if ($this->_cityName === false && $this->getCityModel()) {
            $this->_cityName = $this->getCityModel()->name;
        }
        return $this->_cityName;
    }

    /**
     * @return integer
     */
    public function getDistrict_id()
    {
        if ($this->_district_id === false && $this->map_lng && $this->map_lat) {
            /** @var Location[] $districts */
            $districts = Location::find()
                ->where('FIND_IN_SET(:city_id, parent_ids)', ['city_id' => $this->city_id])
                ->andWhere(['IS NOT', 'polygon', NULL])
                ->andWhere(['is_leaf' => 1]) // поиск по микрорайону
                ->all();
            foreach ($districts as $district) {
                if (Polygon::belongsToPolygon(['lng' => $this->map_lng, 'lat' => $this->map_lat], $district->preparedPolygon)) {
                    $this->_district_id = $district->id;
                    break;
                }
            }
            if ($this->_district_id === false) {
                /** @var Location[] $districts */
                $districts = Location::find()
                    ->where('FIND_IN_SET(:city_id, parent_ids)', ['city_id' => $this->city_id])
                    ->andWhere(['IS NOT', 'polygon', NULL])
                    ->andWhere(['is_leaf' => 0]) // поиск по району
                    ->all();
                foreach ($districts as $district) {
                    if (Polygon::belongsToPolygon(['lng' => $this->map_lng, 'lat' => $this->map_lat], $district->preparedPolygon)) {
                        $this->_district_id = $district->id;
                        break;
                    }
                }
            }
        }
        if ($this->_district_id === false && $this->city_id) {
            /** @var Location $cityLocation */
            $cityLocation = Location::findOne($this->city_id);
            // если несуществует микрорайона, то записывать найденый район
            if (!$cityLocation->getAllChildren()->andWhere(['level' => ($cityLocation->level + 2)])->exists()) {
                $this->_district_id = $this->borough_id;
            }
        }
        if ($this->_district_id === false) {
            list($this->_street_id, $this->_district_id) = StreetContext::findInContext($this->city_id, $this->address_text . ' ' . $this->system_text . ' ' . $this->promo_title);
            if (!$this->_street_id) { // здесь район города на основе улици определяется, по этому если ее нет, то нужно еще раз искать, можно проверять район, но если улица уже есть, то это безсмысленно
                list($this->_street_id, $this->_district_id) = StreetContext::findInContext($this->city_id, $this->promo_text);
            }
        }
        if ($this->_district_id === false) {
            $this->_district_id = null;
        }
        return $this->_district_id;
    }

    /**
     * @return Street
     */
    public function getStreetModel()
    {
        if (!$this->_streetModel && $this->street_id) {
            $this->_streetModel = Street::findOne($this->street_id);
        }
        return $this->_streetModel;
    }

    /**
     * @return string
     */
    public function getStreet_name()
    {
        if (!$this->_street_name && $this->streetModel) {
            $this->_street_name = $this->streetModel->name;
        }
        return $this->_street_name;
    }

    /**
     * @return integer
     */
    public function getStreet_id()
    {
        if ($this->_street_id === false) {
            list($this->_street_id, $this->_district_id) = StreetContext::findInContext($this->city_id, $this->address_text . ' ' . $this->system_text . ' ' . $this->promo_title . ' ' . $this->promo_text);
            if (!$this->_street_id) {
                list($this->_street_id, $this->_district_id) = StreetContext::findInContext($this->city_id, $this->promo_text);
            }
        }
        if ($this->_street_id === false) {
            $this->_street_id = null;
        }
        return $this->_street_id;
    }

    /**
     * @return mixed|null|string
     * находит номер дома в контексте улицы
     */
    public function getHouse_num()
    {
        if (!$this->_house_num) {
            /** @var StreetContext[] $streetContexts */
            $streetContexts = StreetContext::findAll(['convert_id' => $this->street_id]);
            $findIn = $this->address_text . ' ' . $this->system_text . ' ' . $this->promo_title . ' ' . $this->promo_text;

            $this->_house_num = null;

            if ($streetContexts) {
                foreach ($streetContexts as $streetContext) {
                    $this->_house_num = $this->findHouseNum($streetContext->name, $findIn);
                    if ($this->_house_num !== null) break;
                }
            }
            if (!$this->_house_num && $this->street_id) {
                $this->_house_num = $this->findHouseNum($this->streetModel->name, $findIn);
            }
            $this->_house_num = $this->formatHouseNumber($this->_house_num);
        }

        return $this->_house_num;
    }

    /**
     * @param string $text
     * @param string $key возможные занчения: total, life, kitchen
     * @return float|null
     */
    public function findAreaInText($text, $key = 'total')
    {
        if (!array_key_exists($key, $this->_areaFromTextCache)) {
            if (preg_match('#(\d+)\/(\d+)\/(\d+)#ui', $text, $matches)) {
                if ($matches[1] < $matches[2])
                    return null;

                $this->_areaFromTextCache['total'] = $this->formatArea($matches[1]);
                $this->_areaFromTextCache['life'] = $this->formatArea($matches[2]);
                $this->_areaFromTextCache['kitchen'] = $this->formatArea($matches[3]);
            } elseif (preg_match('#Общая площадь\s?-\s?([\d\.]+)\s?м2#ui', $text, $matches)) {
                $this->_areaFromTextCache['total'] = $this->formatArea($matches[1]);
                if (preg_match('#Жилая площадь\s?-\s?([\d\.]+)\s?м2#ui', $text, $matches)) {
                    $this->_areaFromTextCache['life'] = $this->formatArea($matches[1]);
                }
                if (preg_match('#Площадь кухни\s?-\s?([\d\.]+)\s?м2#ui', $text, $matches)) {
                    $this->_areaFromTextCache['kitchen'] = $this->formatArea($matches[1]);
                }
            } elseif (preg_match_all('#(\d+[\.,\s]?\d*)\s?(кв\.м|м\.кв|м2)#ui', $text, $matches)) {
                if (!(count($matches[1]) == 1 || count($matches[1]) == 3))
                    return null;

                $areas = [];
                foreach ($matches[1] as $match) {
                    $areas[] = $this->formatArea($match);
                }

                arsort($areas);
                $this->_areaFromTextCache['total'] = $this->formatArea(array_shift($areas));
                $this->_areaFromTextCache['life'] = $this->formatArea(array_shift($areas));
                $this->_areaFromTextCache['kitchen'] = $this->formatArea(array_shift($areas));
            }
        }
        if (array_key_exists($key, $this->_areaFromTextCache)) {
            return $this->_areaFromTextCache[$key];
        } else {
            return null;
        }
    }

    /**
     * @param string $text
     * @param string $key возможные занчения: current, total
     * @return int|null
     */
    public function findFloorInText($text, $key = 'current')
    {
        if (!array_key_exists($key, $this->_areaFromTextCache)) {
            if (preg_match('#(\d+)\/(\d+)\s(этаж|эт\.)#ui', $text, $matches)) {
                if ($matches[1] > $matches[2])
                    return null;

                $this->_floorFromTextCache['current'] = $this->toInt($matches[1]);
                $this->_floorFromTextCache['total'] = $this->toInt($matches[2]);
            }
        }
        if (array_key_exists($key, $this->_floorFromTextCache)) {
            return $this->_floorFromTextCache[$key];
        } else {
            return null;
        }
    }

    protected function findHouseNum($streetName, $findIn)
    {
        return ParsingTools::findHouseNum($streetName, $findIn);
    }

    /**
     * Работает только если определен номер дома
     * документация здесь @link http://wiki.openstreetmap.org/wiki/Nominatim
     * @return array
     */
    public function getCoordsFromOsm()
    {
        if (!$this->_coordsFromOsm) {
            $this->_coordsFromOsm = ['map_lat' => null, 'map_lng' => null];
            $params = [
                'format' => 'jsonv2',
                'accept-language' => \Yii::$app->language,
                'street' => "{$this->house_num} {$this->street_name}",
                'city' => $this->getCityName(),
                'country' => Yii::$app->params['country'],
            ];

            $url = 'https://nominatim.openstreetmap.org/search?' . http_build_query($params);

            $client = new Client($url, ['timeout' => 20,'sslverifypeer' => false,]);
            $response = $client->send();
            if ($response->getStatusCode() === 200) {
                switch ($params['format']) {
                    case 'xml':
                    case 'html':
                        $result = $response->getBody(); // not supported
                        break;
                    case 'json':
                    case 'jsonv2':
                    default:
                        $result = Json::decode($response->getBody());
                }
                if (isset($result[0]['lat']) && isset($result[0]['lon'])) {
                    $this->_coordsFromOsm = ['map_lat' => $result[0]['lat'], 'map_lng' => $result[0]['lon']];
                }

            }
        }
        return $this->_coordsFromOsm;
    }

    /**
     * @param int $val
     */
    public function setCategory($val)
    {
        $this->_category = $val;
    }

    /**
     * @param int $val
     */
    public function setRegion_id($val)
    {
        $this->_region_id = $val;
    }

    /**
     * @param int $val
     */
    public function setRealty_type($val)
    {
        $this->_realty_type = $val;
    }

    /**
     * @param int $val
     */
    public function setDeal($val)
    {
        $this->_deal = $val;
    }

    /**
     * @param string $val
     * @return float
     */
    public function formatArea($val)
    {
        $val = str_replace(',', '.', $val);
        $val = preg_replace('#[^\d\.]#', '', $val);
        $val = trim($val, " \t\n\r\0\x0B.");
        return floatval(Yii::$app->formatter->asDecimal($val));
    }

    /**
     * @param string $houseNum
     * @return mixed|string
     * приводит номер дома к стардартному виду для точного сравнения.
     */
    public function formatHouseNumber($houseNum)
    {
        return ParsingTools::formatHouseNumber($houseNum);
    }

    /**
     * @deprecated
     *
     * @param $image_url
     * @param null $referer_url
     * @param string $storage
     * @return bool|string
     */
    public function saveImage($image_url, $referer_url = null, $storage = 'amazon')
    {
        $file = new \SplFileInfo($image_url);
        $file->getExtension();

        $client = $this->connection->getClient($image_url, false, ['timeout' => 5,'sslverifypeer' => false,]);
        $headers = [
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Upgrade-Insecure-Requests' => '1',
        ];
        if ($referer_url) {
            $headers['Referer'] = $referer_url;
        }
        $client->setHeaders(ArrayHelper::merge($this->connection->headers, $headers));
        $response = $this->connection->trySend($client);

        if ($storage == 'amazon') {
            $path = date('Y') . '/' . date('m') . '/' . md5($image_url) . '.' . $file->getExtension();
            return Yii::$app->get('amazons3')->putImage($response->getBody(), $path);
        } elseif ($storage == 'local') {
            $subPath = date('Y') . '/' . date('m');
            $path = Yii::getAlias('@storage/' . $subPath);
            FileHelper::createDirectory($path);
            $name = md5($image_url) . '.' . $file->getExtension();
            if (!file_exists($path . '/' . $name))
                file_put_contents($path . '/' . $name, $response->getBody());
            return $subPath . '/' . $name;
        }
        return false;
    }
}
