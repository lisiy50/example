<?php

namespace app\parsing\parser;

use DiDom\Document;
use yii\base\Exception;
use yii\base\ExitException;
use \yii\base\BaseObject;

/**
 * Class BaseListObject
 * @package app\parser
 *
 * Magic Properties:
 * @property string $parsing_portal_id
 * @property string $url
 * @property string $object_key
 * @property int $price
 * @property int $price_currency_id
 * @property string $price_kind
 * @property int $created_at
 * @property int $updated_at
 * @property string $content
 * @property Document $document
 * @property bool $isValid
 *
 */
abstract class BaseListObject extends BaseObject
{
    /** @var string */
    protected $_url;
    /** @var string */
    protected $_object_key;
    /** @var int */
    protected $_price;
    /** @var int */
    protected $_price_currency_id;
    /** @var string */
    protected $_price_kind;
    /** @var int */
    protected $_created_at;
    /** @var int */
    protected $_updated_at;
    /** @var string */
    protected $_content;
    /** @var Document */
    protected $_document;
    /** @var int */
    protected $_price_per_object;
    /** @var int */
    protected $_price_per_sqr;

    /** @return string */
    public abstract function getParsing_portal_id();

    /** @return string */
    public abstract function getUrl();

    /**
     * @param string $url
     * @return string
     */
    public abstract function prepareUrl(string $url);

    /** @return int */
    public abstract function getPrice();

    /** @return int */
    public abstract function getPrice_currency_id();

    /** @return string */
    public abstract function getPrice_kind();

    /** @return int */
    public abstract function getCreated_at();

    /** @return int */
    public abstract function getUpdated_at();

    /** @return string */
    public abstract function findObject_key();

    /** @return bool */
    public abstract function getIsValid();

    /**
     * BaseListObject constructor.
     * @param string $content
     * @param array $config
     */
    public function __construct($content, array $config = [])
    {
        $this->_content = $content;
        parent::__construct($config);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getContent()
    {
        if (!$this->_content) {
            throw new Exception('Необходимо присвоить значение $content.');
        }
        return $this->_content;
    }

    /**
     * @return Document
     */
    public function getDocument()
    {
        if (!$this->_document) {
            $this->_document = new Document($this->content);
        }
        return $this->_document;
    }

    /**
     * возвращает ключ объекта если задана ссылка на объект, если нет, то выбрасывает ексепшин
     * @return string
     * @throws ExitException
     */
    public function getObject_key()
    {
        if (!$this->_object_key) {
            if ($this->url) {
                $this->_object_key = $this->findObject_key();
            } else {
                throw new ExitException(0, 'Необходимо указать ссылку на объект');
            }
        }
        return $this->_object_key;
    }

    /**
     * @param string $string
     * @param string|array $words
     * @return bool
     */
    protected function contain($string, $words)
    {
        $words = is_array($words) ? $words : [$words];
        foreach($words as $word) {
            if(mb_strpos($string, $word, null, 'utf-8') !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $number
     * @return int
     */
    protected function toInt($number)
    {
        return intval(preg_replace('#[^\d]#u', '', $number));
    }
}
