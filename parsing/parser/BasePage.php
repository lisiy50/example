<?php

namespace app\parsing\parser;
use DiDom\Document;
use DiDom\Element;

/**
 * Class BasePage
 * @package app\parser
 */
abstract class BasePage extends Base
{
    /**
     * @param string $url
     * @param int $page
     * @return string
     */
    public abstract function getPageUrl(string $url, int $page);

    /**
     * @param string $url
     * @return int
     */
    public abstract function getTotalPages(string $url);

    /**
     * @param string $url
     * @param int $page
     * @return array
     */
    public abstract function getObjectList(string $url, int $page);

    /**
     * возвращает массив обектов на основе ссылки что в свойстве $url
     * @return BaseListObject[]
     */
    public function getObjectListCurrent()
    {
        $result = [];
        $client = $this->connection->getClient($this->url, true);
        $html = $this->connection->trySend($client)->getBody();
        $document = new Document($html);
        /** @var Element $item */
        foreach($document->find('.realty-object-card') as $item) {
            $result[] = new Realty100ListObject($item->html());
        }
        return $result;
    }
}
